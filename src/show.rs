use super::info::Device;
use std::path::Path;

//#[pyfunction]
pub fn print(id:u32,name:&str,path:&Path) {
	let device = Device::new(id, name, path);
	println!("{}", device);
	println!("{}", device.memory_info());
	println!("{}", device.json());
}
