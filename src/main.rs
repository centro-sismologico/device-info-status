use device_status::show::print;
use std::path::{Path,PathBuf};
use clap::Parser;
use std::fs::File;
use std::io::{ self, BufRead};

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct InfoDeviceArgs {
    /// Name of the person to greet
    #[arg(short, long)]
    id: u32,
    #[arg(short, long)]
    name: String,
    #[arg(short, long)]
    path: PathBuf,
	#[arg(short, long)]
	list:bool
}



fn main() {
	let args = InfoDeviceArgs::parse();
	if args.path.exists() {
		match args {
			InfoDeviceArgs {path, list:true, name,id}=>{
				let file = File::open(path).unwrap(); 
				for line in io::BufReader::new(file).lines() {
					let pb = Path::new(&line.unwrap().to_string()).to_path_buf();
					print(id, &name, &pb);
				}				
			},
			InfoDeviceArgs {path, list:false, name,id}=>{
				print(id, &name, &path);
			},
		}
	} else {
		println!("No existe la ruta {:?}", args.path);
	}
}
